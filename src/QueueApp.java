import javax.swing.JFrame;
import javax.swing.text.LabelView;

import java.awt.Font;
import java.util.LinkedList;
import java.awt.event.*;

import javax.swing.*;

public class QueueApp extends JFrame {
    JTextField txtName;
    JLabel lblQueuelist, lblCurrent;
    JButton btnAddQueue, btnGetQueue, btnClearQueue;
    LinkedList<String> queue;

    public QueueApp() {
        super("Queue App");
        queue = new LinkedList();
        this.setSize(400, 300);
        txtName = new JTextField();
        txtName.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
               AddQueue();
                
            }
            
        });
        txtName.setBounds(30, 10, 200, 20);

        btnAddQueue = new JButton("Add Queue");
        btnAddQueue.setBounds(250, 10, 100, 20);
        btnAddQueue.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                AddQueue();
            }

        });

        btnGetQueue = new JButton("Get Queue");
        btnGetQueue.setBounds(250, 40, 100, 20);
        btnGetQueue.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                GetQueue();
            }

        });

        btnClearQueue = new JButton("Clear Queue");
        btnClearQueue.setBounds(250, 70, 100, 20);
        btnClearQueue.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                ClearQueue();
            }

        });

        lblQueuelist = new JLabel("Empty");
        lblQueuelist.setBounds(30, 40, 200, 20);

        lblCurrent = new JLabel("?");
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        lblCurrent.setFont(new Font("Serif", Font.PLAIN, 50));
        lblCurrent.setBounds(30, 70, 200, 50);

        this.add(lblCurrent);
        this.add(txtName);
        this.add(txtName);
        this.add(btnAddQueue);
        this.add(btnGetQueue);
        this.add(btnClearQueue);
        this.add(lblQueuelist);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        showQueue();
        this.setVisible(true);
    }

    public void showQueue() {
        if (queue.isEmpty()) {
            lblQueuelist.setText("Empty");
        } else {
            lblQueuelist.setText(queue.toString());
        }
    }

    public void AddQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }
        queue.add(name);
        txtName.setText("");
        showQueue();
    }

    public void GetQueue() {
        if(queue.isEmpty()) {
            lblCurrent.setText("?");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void ClearQueue() {
        queue.clear();
        lblCurrent.setText("?");
        txtName.setText("");
        showQueue();
    }

    public static void main(String[] args) {
        QueueApp frame = new QueueApp();
    }
}
