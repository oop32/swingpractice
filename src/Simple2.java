import javax.swing.JButton;
import javax.swing.JFrame;
class MyFrame extends JFrame{    
    JButton button;
    public MyFrame() {
        super();
        this.setTitle("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 50);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
}
public class Simple2 {
    public static void main(String[] args) {
        JFrame frame = new MyFrame();
        frame.setVisible(true);
    }
}
